import java.net.InetAddress;

public class Player {
        private InetAddress senderAddress;
        private int senderPort;
        private byte senderId;
        private boolean readiness;
        private boolean start;
        private int comboNum;
        private boolean comboRec;

        public Player(){
            senderId = 0;
            readiness = false;
            comboNum = -1;
            comboRec = false;
        }

        public Player(InetAddress Address, int Port, byte id){
            senderAddress = Address;
            senderPort = Port;
            senderId = id;
        }

        public void setAddress(InetAddress Address){
            senderAddress = Address;
        }

        public InetAddress getAddress(){
            return senderAddress;
        }

        public void setPort(int Port){
            senderPort = Port;
        }

        public int getPort(){
            return senderPort;
        }

        public void setId(byte id) { senderId = id; }

        public byte getId() { return senderId; }

        public void setReadiness(boolean ready) { readiness = ready; }

        public boolean getReadiness() { return readiness; }

        public void setStart(boolean s) { start = s; }

        public boolean getStart() { return start; }

        public void setComboNum(int num) { comboNum = num; }

        public int getComboNum() { return comboNum; }

        public void setComboRec(boolean c) { comboRec = c; }

        public boolean getComboRec() { return comboRec; }


}
