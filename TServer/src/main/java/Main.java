import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;


public class Main {
    public static void main(String args[]) {
        Server server = new Server();
        ArrayList<Player> players = new ArrayList<>();
        for(int i = 0; i <= 4; i++){
            Player player = new Player();
            players.add(i, player);
        }
        byte id = 0;
        byte start = 0;
        byte[] dataStart;
        while(true)
        {
            server.receivingUTP();
            byte[] data = server.getReceivingDataBufferUDP();
            if(data[0] == 0x00 && id <= 4 && (start != id || id == 0) && start != -1) {
                id++;
                if(!server.addressPortUDP(players, id)) {
                    server.joiningGameUDP(players.get(id), id);
                }
            }
            if (data[0] == 0x01) {
                for (byte i = 1; i <= 4; i++) {
                    if(data[1] != i && players.get(i).getId() != 0){
                        server.playerField(players.get(i));
                    }
                }
            }
            if(data[0] == 0x02) {
                server.comboReceived(players.get(data[1]));
                byte[] dataCombo = new byte[10];
                byte[] arr = new byte[4];
                for(int k = 2; k < 6; k++){
                    arr[k-2] = data[k];
                }
                for(int k = 0; k < 10; k++){
                    dataCombo[k] = data[k];
                }
                ByteBuffer wrapped = ByteBuffer.wrap(arr);
                int numCom = wrapped.getInt();
                if(players.get(dataCombo[1]).getComboNum() != numCom) {
                    players.get(dataCombo[1]).setComboNum(numCom);
                    for (byte i = 1; i <= 4; i++) {
                        if (dataCombo[1] != i && players.get(i).getId() != 0) {
                            server.combo(players.get(i), dataCombo);
                            long startTime = System.currentTimeMillis();
                            long elapsedTime = 0L;
                            long startTimeCombo = System.currentTimeMillis();
                            long elapsedTimeCombo = 0L;
                            while (!players.get(i).getComboRec() && elapsedTime < 2*1000) {
                                server.receivingUTP();
                                elapsedTime = (new Date()).getTime() - startTime;
                                elapsedTimeCombo = (new Date()).getTime() - startTimeCombo;
                                byte[] data1 = server.getReceivingDataBufferUDP();
                                if (data1[0] == 0x03 && data1[1] == i){
                                    players.get(i).setComboRec(true);
                                }else {
                                    if(elapsedTimeCombo >= 200) {
                                        server.combo(players.get(i), data);
                                        elapsedTimeCombo = 0L;
                                    }
                                }
                            }
                            if(!players.get(i).getComboRec()){
                                players.get(i).setId((byte) 0);
                            }
                        }
                    }
                    for (byte i = 1; i <= 4; i++) {
                        players.get(i).setComboRec(false);
                    }
                }
            }

            if(start != id && start != -1) {
                if(data[0] == 0x04) {
                    for (byte i = 1; i <= 4; i++) {
                        if (data[1] == i && !players.get(i).getReadiness()) {
                            players.get(i).setReadiness(true);
                            start++;
                        }
                    }
                }
            }
            if(start == id && start != -1){
                for(byte i = 1; i <= id; i++) {
                    dataStart = data;
                    while (!players.get(i).getStart()) {
                        server.start(players.get(i));
                        if (dataStart[0] == 0x05 && dataStart[1] == i) {
                            players.get(i).setStart(true);
                        }else{
                            server.start(players.get(i));
                        }
                        server.receivingUTP();
                        dataStart = server.getReceivingDataBufferUDP();
                    }
                }
                start = -1;
            }

        }
    }
}
