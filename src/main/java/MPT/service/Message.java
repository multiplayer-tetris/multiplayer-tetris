package MPT.service;

public class Message {
    private final byte msgType;
    private Object data = null;

    public Message(byte msgType, Object data){
        this.msgType = msgType;
        this.data = data;
    }

    public byte getMsgType(){
        return msgType;
    }

    public Object getData(){
        return data;
    }
}
