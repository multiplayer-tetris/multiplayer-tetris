package MPT;

public class IPUtil {
    public static boolean validIP(String ip) {
        if (ip == null || ip.isEmpty()) {
            return false;
        }

        if (ip.endsWith(".")) {
            return false;
        }

        String[] parts = ip.split("\\.");
        if (parts.length != 4) {
            return false;
        }

        for (String s : parts) {
            int i = Integer.parseInt(s);
            if ((i < 0) || (i > 255)) {
                return false;
            }
        }

        return true;
    }
}
