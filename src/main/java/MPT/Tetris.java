package MPT;

import MPT.model.Model;

import java.net.InetAddress;

class Tetris {
    Model model;
    private View view;
    private Controller controller;

    public Tetris(InetAddress serverAddress) throws Exception {
        model = new Model(serverAddress);
        view = new View(model);
        controller = new Controller(model, view);
    }

    void start(){
        controller.start();
    }

    public Model getModel() {
        return model;
    }
}
