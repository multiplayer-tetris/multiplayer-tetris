package MPT;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import MPT.model.Model;

import static java.lang.System.exit;

class Controller {
    private Model model;
    private View view;

    Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    void start() {
        model.newGame();
        view.setupGUI();
        view.setVisible();

        view.about.addActionListener(actionEvent ->
        {
            view.aboutFrame.setVisible(true);
        });

        view.exit.addActionListener(actionEvent -> exit(0));

        view.tetris.setFocusable(true);
        view.tetris.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                switch (e.getKeyCode()) {
                    case (KeyEvent.VK_Z):
                        if (!model.getPauseStatus() && !model.getInternalPauseStatus() && !model.getGameOverStatus())
                            model.rotate(false);
                        break;
                    case (KeyEvent.VK_X):
                    case (KeyEvent.VK_UP):
                        if (!model.getPauseStatus() && !model.getInternalPauseStatus() && !model.getGameOverStatus())
                            model.rotate(true);
                        break;
                    case (KeyEvent.VK_DOWN):
                        if (!model.getPauseStatus() && !model.getInternalPauseStatus() && !model.getGameOverStatus())
                            model.movedown();
                        break;
                    case (KeyEvent.VK_LEFT):
                        if (!model.getPauseStatus() && !model.getInternalPauseStatus() && !model.getGameOverStatus())
                            model.moveleft();
                        break;
                    case (KeyEvent.VK_RIGHT):
                        if (!model.getPauseStatus() && !model.getInternalPauseStatus() && !model.getGameOverStatus())
                            model.moveright();
                        break;
                    case (KeyEvent.VK_SPACE):
                        if (!model.getPauseStatus() && !model.getInternalPauseStatus() && !model.getGameOverStatus())
                            model.drop();
                        break;
                    default: {
                    }
                }
            }
        });
    }
}
