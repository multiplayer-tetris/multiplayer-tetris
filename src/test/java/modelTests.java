import MPT.model.*;
import MPT.service.Receiver;
import MPT.service.Sender;
import MPT.tetrominos.Tetromino;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.awt.Color;

import static org.junit.jupiter.api.Assertions.*;


public class modelTests {
    private Model model;
    private MovableTetromino mt;
    private TetrisRNG rng;
    private TetrominoFactory factory;

    @Before
    public void setUp() {
        rng = new TetrisRNG(System.currentTimeMillis());
    }

    @Test
    @DisplayName("RNG test")
    public void testRNG() {
        LinkedList<Integer> testData;
        LinkedList<Integer> testPerm = new LinkedList<>();
        for (int i = 0; i < 1000; i++) {
            testData = rng.getSequence();
            assertEquals(7, testData.size());
            for (int j = 0; j < testData.size(); j++) {
                if (testData.get(j) < 0 || testData.get(j) > 6 || testPerm.contains(testData.get(j)))
                    fail();
                testPerm.add(testData.get(j));
            }
            testPerm.clear();
        }
    }

    @Before
    public void setModel() {
        model = new Model();
    }

    @Test
    @DisplayName("Model creation")
    public void testModelCreate() {
        assertEquals(0, model.getTo_junk());
        assertEquals(0, model.getFrom_junk());
        assertFalse(model.getReady());
        assertFalse(model.getGameOverStatus());

        assertEquals(java.util.LinkedList.class, model.getPlayers().getClass());
        assertEquals(java.util.HashMap.class, model.getHashMapPlayers().getClass());
        assertEquals(java.util.LinkedList.class, model.getColoredboard().getClass());
        assertEquals(java.util.LinkedList.class, model.getBoarddata().getClass());
        assertEquals(model.getHeight(), model.getColoredboard().size());
        assertEquals(model.getWight(), model.getColoredboard().get(0).length);
        assertEquals(java.util.HashMap.class, model.getColorByte().getClass());
        assertEquals(java.util.Vector.class, model.getCombos().getClass());

        HashMap<Color, Byte> testByte = new HashMap<>();
        for (int i = 0; i < model.getColors().length; ++i)
            testByte.put(model.getColors()[i], (byte) i);
        testByte.put(null, (byte) model.getColors().length);

        assertEquals(testByte, model.getColorByte());
    }


    @Test
    @DisplayName("Server Model")
    public void testServerModel() {
        try {
            model = new Model(InetAddress.getByName("127.0.0.1"));
        } catch (Exception e) {
            assertEquals("Couldn't connect to server", e.getMessage());
        }
    }

    @Before
    public void setKey() {
        model = new Model();
        long key = System.currentTimeMillis();
        model.setKey(key);
        model.setTetrisRNG(new TetrisRNG(key));
        try {
            model.setSender(new Sender(InetAddress.getByName("127.0.0.1"), new DatagramSocket()));
        } catch (SocketException | UnknownHostException e) {
            e.printStackTrace();
        }
        factory = new TetrominoFactory();
    }

    @Test
    @DisplayName("New game")
    public void new_game() {
        model.newGame();
        assertEquals(0, model.getLevel());
        assertEquals(0, model.getCounter());
        assertEquals(0, model.getScore());
        assertEquals(10, model.getGoal());
        assertEquals(20, model.getNextGoal());

        assertEquals(java.util.LinkedList.class, model.getColoredboard().getClass());
        assertEquals(20, model.getColoredboard().size());
        assertEquals(10, model.getColoredboard().get(0).length);

        assertFalse(model.getGameOverStatus());
        assertFalse(model.getPauseStatus());

        mt = new MovableTetromino(factory.getTetromino(model.getId()), model.getSpawn());
        assertTrue(Arrays.deepEquals(mt.getShape(), model.getMovablePiece().getShape()));

        Tetromino nextPiece = factory.getTetromino(model.getNextId());

        assertEquals(nextPiece.getClass(), model.getNextPiece().getClass());
        assertFalse(model.getInternalPauseStatus());
    }

    @Before
    public void setUpMovementTest() {
        model = new Model();
        factory = new TetrominoFactory();
        rng = new TetrisRNG(System.currentTimeMillis());
    }

    @Test
    @DisplayName("Movement test")
    public void movementTest() {
        try {
            model.setReceiver(new Receiver(new DatagramSocket()));
        } catch (SocketException e) {
            e.printStackTrace();
        }

        mt = new MovableTetromino(factory.getTetromino(0), model.getSpawn());
        model.setMovablePiece(mt);
        model.setNextPiece(factory.getTetromino(0));
        model.setIds(rng.getSequence());

        model.movedown();
        assertEquals(model.getSpawn().getX(), model.getMovablePiece().getPosition().getX());
        assertEquals(model.getSpawn().getY() + 1, model.getMovablePiece().getPosition().getY());


        for (int i = 0; i < 8; i++) {
            model.moveleft();
            if (i < 3)
                assertEquals(model.getSpawn().getX() - (i + 1), model.getMovablePiece().getPosition().getX());
            else
                assertEquals(model.getSpawn().getX() - 3, model.getMovablePiece().getPosition().getX());
            assertEquals(model.getSpawn().getY() + 1, model.getMovablePiece().getPosition().getY());
        }

        for (int i = 0; i < 8; i++) {
            model.moveright();
            if (i < 6)
                assertEquals(model.getSpawn().getX() - 3 + (i + 1), model.getMovablePiece().getPosition().getX());
            else
                assertEquals(model.getSpawn().getX() + 3, model.getMovablePiece().getPosition().getX());
            assertEquals(model.getSpawn().getY() + 1, model.getMovablePiece().getPosition().getY());
        }

        model.drop();
        assertEquals(model.getSpawn().getX(), model.getMovablePiece().getPosition().getX());
        assertEquals(0, model.getMovablePiece().getPosition().getY());
        LinkedList<Color[]> field = model.getColoredboard();
        Color[] line = {null, null, null, null, null, null, Color.CYAN, Color.CYAN, Color.CYAN, Color.CYAN};

        assertTrue(Arrays.deepEquals(field.get(field.size() - 1), line));
    }

    @Before
    public void setUpRotationTest() {
        model = new Model();
        factory = new TetrominoFactory();
        try {
            model.setSender(new Sender(InetAddress.getByName("127.0.0.1"), new DatagramSocket()));
        } catch (SocketException | UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Rotation test")
    public void rotationTest() {

        for (int j = 0; j<7; j++) {
            mt = new MovableTetromino(factory.getTetromino(j), model.getSpawn());
            model.setMovablePiece(mt);

            for (int i = 0; i < 1000; i++) {
                model.rotate(true);
                mt.setState(mt.getState() + 1);
                assertTrue(Arrays.deepEquals(mt.getShape(), model.getMovablePiece().getShape()));
            }

            for (int i = 0; i < 1000; i++) {
                model.rotate(false);
                if (mt.getState() == 0)
                    mt.setState(3);
                else
                    mt.setState(mt.getState() - 1);
                assertTrue(Arrays.deepEquals(mt.getShape(), model.getMovablePiece().getShape()));
            }
            assertTrue(Arrays.deepEquals(mt.getShape(), model.getMovablePiece().getShape()));
        }
    }

    @Before
    public void setUpJunkTest() {
        model = new Model();
        factory = new TetrominoFactory();
        rng = new TetrisRNG(System.currentTimeMillis());
        try {
            model.setSender(new Sender(InetAddress.getByName("127.0.0.1"), new DatagramSocket()));
        } catch (SocketException | UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Junk Test")
    public void junkTest() {
        try {
            model.setReceiver(new Receiver(new DatagramSocket()));
        } catch (SocketException e) {
            e.printStackTrace();
        }

        mt = new MovableTetromino(factory.getTetromino(0), model.getSpawn());
        model.setMovablePiece(mt);
        model.setNextPiece(factory.getTetromino(0));
        model.setFrom_junk(2);
        model.setIds(rng.getSequence());
        model.drop();

        LinkedList<Color[]> field = model.getColoredboard();
        Color[] line = {null, null, null, Color.CYAN, Color.CYAN, Color.CYAN, Color.CYAN, null, null, null};
        assertTrue(Arrays.deepEquals(field.get(field.size() - 3), line));
    }

    @Before
    public void setUpClearTest() {
        model = new Model();
        factory = new TetrominoFactory();
        rng = new TetrisRNG(System.currentTimeMillis());
        try {
            model.setSender(new Sender(InetAddress.getByName("127.0.0.1"), new DatagramSocket()));
        } catch (SocketException | UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Clear Test")
    public void clearTest() {
        try {
            model.setReceiver(new Receiver(new DatagramSocket()));
        } catch (SocketException e) {
            e.printStackTrace();
        }

        LinkedList<Integer> ids = new LinkedList<>();
        for (int i = 0; i<11; i++)
            ids.add(0);
        model.setIds(ids);

        mt = new MovableTetromino(factory.getTetromino(0), model.getSpawn());
        model.setMovablePiece(mt);
        model.setNextPiece(factory.getTetromino(0));

        for (int i=0; i<4; i++) {
            for (int j=0; j<4; j++) {
                model.moveleft();
            }
            model.drop();
        }

        for (int i=0; i<4; i++) {
            model.moveright();
            model.drop();
        }

        for (int i=0; i<2; i++) {
            for (int j = 0; j < 4; j++) {
                model.moveright();
            }
            model.rotate(true);
            if (i == 1)
                model.moveright();
            model.drop();
        }

        model.drop();

        LinkedList<Color[]> field = model.getColoredboard();
        Color[] line = {null, null, null, Color.CYAN, Color.CYAN, Color.CYAN, Color.CYAN, null, null, null};
        assertTrue(Arrays.deepEquals(field.get(field.size() - 1), line));
    }


}