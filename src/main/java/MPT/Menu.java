package MPT;

import MPT.service.Message;
import MPT.service.Protocol;

import javax.swing.*;
import java.awt.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

import static java.lang.System.exit;

class Menu {
    Tetris tetris;
    JButton ready;

    void menu() {
        JFrame frame = new JFrame("Tetris Menu");
        frame.setPreferredSize(new Dimension(300, 400));
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        JFrame aboutFrame = new JFrame("About");
        aboutFrame.setSize(630,250);
        aboutFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        JPanel aboutPanel = new JPanel();
        aboutPanel.setLayout(new BoxLayout(aboutPanel, BoxLayout.Y_AXIS));
        aboutPanel.setFocusable(false);
        aboutPanel.setSize(630,250);
        JTextArea aboutTextArea = new JTextArea();
        aboutTextArea.setFont(new Font ("monospaced", Font.BOLD, 14));
        aboutTextArea.append("This is Tetris. But this time it's multiplayer.\n\n" +
                "Controls:\n" +
                "Z - Counter Clockwise Turn       X/UP ARROW - Clockwise Turn\n" +
                "DOWN ARROW - Soft Drop           LEFT/RIGHT ARROWS - Move Left/Right\n" +
                "SPACE - Hard drop                ESCAPE - Pause/Unpause\n\n\n\n" +
                "Made by Maseevsky Anton, Mironov Vladimir and Stryapkov Kirill as a student task at NSU\n\n\n");
        aboutFrame.add(aboutTextArea);

        JPanel menupanel = new JPanel();
        menupanel.setPreferredSize(new Dimension(200, 300));

        ready = new JButton("Ready");
        ready.setPreferredSize(new Dimension(200, 50));
        ready.addActionListener(actionEvent -> {
            tetris.getModel().getSender().putMessage(new Message(Protocol.READINESS, null));
            tetris.start();
            frame.setVisible(false);
            aboutFrame.setVisible(false);
        });

        JButton connect = new JButton("Connect");
        connect.setPreferredSize(new Dimension(200, 50));
        connect.addActionListener(actionEvent -> {
            String ip;
            ip = JOptionPane.showInputDialog("Enter server ip");
            if (IPUtil.validIP(ip)) {
                try {
                    tetris = new Tetris(InetAddress.getByName(ip));
                    menupanel.add(ready);
                    menupanel.updateUI();
                    frame.repaint();
                    menupanel.remove(connect);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                catch (Exception e){
                    JOptionPane.showMessageDialog(null, "Coudn't connect to " + ip);
                    return;
                }
            }
            else {
                JOptionPane.showMessageDialog(null, "Invalid IP address");
            }
        });

        JButton about = new JButton( ("About"));
        about.setPreferredSize(new Dimension(200, 50));
        about.addActionListener(actionEvent -> {
            aboutFrame.setVisible(true);
        });

        JButton exit = new JButton("Exit");
        exit.setPreferredSize(new Dimension(200, 50));
        exit.addActionListener(actionEvent -> exit(0));

        menupanel.add(connect);
        menupanel.add(about);
        menupanel.add(exit);

        frame.add(menupanel, BorderLayout.CENTER);
        frame.pack();

        frame.setVisible(true);
    }
}
