package MPT.service;

import javafx.util.Pair;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.concurrent.LinkedBlockingQueue;

public class Sender extends Thread{
    private final LinkedBlockingQueue<Message> toSend;
    private final byte buf[];
    private final DatagramSocket socket;
    private InetAddress serverAddress = null;
    private int bufLength = 0;
    private final static int SERVER_PORT = 7000;
    private final Thread thisThread;
    private byte gameId;

    public Sender(InetAddress serverAddress, DatagramSocket socket) throws SocketException {
        toSend = new LinkedBlockingQueue<>();
        buf = new byte[1 << 16];
        this.socket = socket;
        this.serverAddress = serverAddress;
        thisThread = Thread.currentThread();
    }

    public void putMessage(Message msg){
        if (msg == null)
            return;
        toSend.add(msg);
    }

    private void toBytes(Message msg){
        if (msg == null)
            return;
        buf[0] = msg.getMsgType();
        switch (msg.getMsgType()){
            case Protocol.PLAYERS_FIELD:{
                byte[] field = (byte[])msg.getData();
                bufLength = field.length;
                System.arraycopy(field, 0, buf, 0, bufLength);
                buf[1] = this.gameId;
                break;
            }
            case Protocol.COMBO:{
                bufLength = 10;
                buf[1] = this.gameId;
                Pair<Integer, Integer> tmp = (Pair<Integer, Integer>) msg.getData();
                int comboNum = tmp.getKey();
                int lineCnt = tmp.getValue();
                byte[] comboNumBuf = ByteBuffer.allocate(4).putInt(comboNum).array();
                byte[] lineCntBuf = ByteBuffer.allocate(4).putInt(lineCnt).array();
                System.arraycopy(comboNumBuf, 0, buf, 2, 4);
                System.arraycopy(lineCntBuf, 0, buf, 6, 4);
                break;
            }
            case Protocol.COMBO_RECVD:{
                bufLength = 2;
                buf[1] = this.gameId;
                break;
            }
            case Protocol.READINESS:{
                bufLength = 2;
                buf[1] = this.gameId;
                break;
            }
            case Protocol.JOIN:{
                bufLength = 1;
                break;
            }
            case Protocol.START_GAME:{
                bufLength = 2;
                buf[1] = this.gameId;
                break;
            }
        }
    }

    public void kill(){
        this.interrupt();
    }

    public void setGameId(byte id){
        this.gameId = id;
    }

    @Override
    public void run(){
        while (!isInterrupted()){
            try {
                // 1. Wait for messages to send
                Message msg = toSend.take();
                if (msg.getMsgType() == MPT.service.Protocol.KILL_SENDER)
                    break;

                // 2. Convert to buffer to send
                toBytes(msg);

                // 3. Form packet and send
                DatagramPacket packet = new DatagramPacket(buf, 0, bufLength);
                packet.setAddress(serverAddress);
                packet.setPort(SERVER_PORT);
                socket.send(packet);
            }
            catch (InterruptedException e){
                break;
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}