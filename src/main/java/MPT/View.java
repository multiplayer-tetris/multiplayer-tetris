package MPT;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.LinkedList;

import MPT.model.Model;

public class View {
    private JFrame tetrisFrame;
    JFrame aboutFrame;
    //    JButton changelevel;
    JButton about;
    JButton exit;
    private Model model;
    private JLabel score;
    private JLabel level;
    private JLabel lines;
    private JLabel nextlvl;
    JPanel sidepanel;
    JPanel clientPanel;
    View.TetrisPanel tetris;
    JLabel gameOver;
    View.ServerPlayerPanel[] sp;
    JLabel[] serverScores;
    JPanel serverPanel;
    private View.NextPiecePanel next;
    private int playerssize;

    public View(Model model) {
        tetrisFrame = new JFrame("Tetris");
        aboutFrame = new JFrame("About");
        this.model = model;
    }

    public void setupGUI() {
        tetrisFrame.setSize(570, 650);
        tetrisFrame.setMinimumSize(new Dimension(910, 650));
        tetrisFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        clientPanel = new JPanel();
        tetrisFrame.setLayout(new BorderLayout());
        tetris = new View.TetrisPanel();

        next = new View.NextPiecePanel();
        score = new JLabel("Score: " + model.getScore());
        level = new JLabel("Level: " + (model.getLevel() + 1));
        lines = new JLabel("Lines: " + model.getCounter());
        nextlvl = new JLabel("Next level: " + model.getGoal() + " lines");

        about = new JButton(("About"));
        about.setFocusable(false);
        about.setPreferredSize(new Dimension(60, 20));

        exit = new JButton("Exit");
        exit.setFocusable(false);
        exit.setPreferredSize(new Dimension(60, 20));


        sidepanel = new JPanel();
        sidepanel.setFocusable(false);
        sidepanel.setLayout(new BoxLayout(sidepanel, BoxLayout.Y_AXIS));
        sidepanel.add(next);
        sidepanel.add(score);
        sidepanel.add(level);
        sidepanel.add(lines);
        sidepanel.add(nextlvl);
        sidepanel.add(about);
        sidepanel.add(exit);
        sidepanel.setPreferredSize(new Dimension(250, 600));
        sidepanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        gameOver = new JLabel("Game Over");

        tetrisFrame.setLocationRelativeTo(null);

        clientPanel.setLayout(new BorderLayout());
        clientPanel.add(tetris, BorderLayout.CENTER);
        clientPanel.add(sidepanel, BorderLayout.EAST);
        clientPanel.setPreferredSize(new Dimension(tetrisFrame.getWidth(), tetrisFrame.getHeight()));
        tetrisFrame.add(clientPanel, BorderLayout.CENTER);
        clientPanel.repaint();

        sp = new View.ServerPlayerPanel[3];
        serverScores = new JLabel[3];
        serverPanel = new JPanel();
        serverPanel.setLayout(new BoxLayout(serverPanel, BoxLayout.Y_AXIS));
        playerssize = model.getPlayers().size();
        for (int i = 0; i < model.getPlayers().size(); i++) {
            sp[i] = new View.ServerPlayerPanel(model.getPlayers().get(i).getId());
            sp[i].setAlignmentX(Component.LEFT_ALIGNMENT);
            serverScores[i] = new JLabel("Player "+i+": "+sp[i].score);
            serverScores[i].setAlignmentX(Component.CENTER_ALIGNMENT);
            serverPanel.add(serverScores[i]);
            serverPanel.add(sp[i]);
        }
        tetrisFrame.add(serverPanel, BorderLayout.WEST);

        aboutFrame.setSize(630, 400);
        aboutFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        JPanel aboutPanel = new JPanel();
        aboutPanel.setLayout(new BoxLayout(aboutPanel, BoxLayout.Y_AXIS));
        aboutPanel.setFocusable(false);
        aboutPanel.setSize(630, 400);
        JTextArea aboutTextArea = new JTextArea();
        aboutTextArea.setFont(new Font("monospaced", Font.BOLD, 14));
        aboutTextArea.append("This is Tetris. But this time it's multiplayer.\n\n" +
                "Controls:\n" +
                "Z - Counter Clockwise Turn       X/UP ARROW - Clockwise Turn\n" +
                "DOWN ARROW - Soft Drop           LEFT/RIGHT ARROWS - Move Left/Right\n" +
                "SPACE - Hard drop                ESCAPE - Pause/Unpause\n\n\n\n" +
                "Made by Maseevsky Anton, Mironov Vladimir and Stryapkov Kirill as a student task at NSU\n\n\n");
        aboutTextArea.append("If you want to see controls and play\njust click on window with\nthe gameboard and press ESCAPE");
        aboutFrame.add(aboutTextArea);
    }

    public void setVisible(){
        tetrisFrame.setVisible(true);
    }

    class TetrisPanel extends JPanel implements PropertyChangeListener {
        int blocksize = 30;
        LinkedList<Color[]> board = new LinkedList<>();

        TetrisPanel() {
            super();
            for (int h = 0; h < model.getHeight(); h++)
                board.add(new Color[model.getWight()]);
            setPreferredSize(new Dimension(300, 600));
            model.addObserver(this);
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D graphics2D = (Graphics2D) g;
            setPreferredSize(new Dimension((tetrisFrame.getWidth()) / 3, tetrisFrame.getHeight() - 50));
            clientPanel.setPreferredSize(new Dimension((tetrisFrame.getWidth()) / 3, (tetrisFrame.getWidth() / 3)*2));
            if ((tetrisFrame.getWidth()) / 3 / 10 < (tetrisFrame.getHeight()-50) /20)
                blocksize = (tetrisFrame.getWidth()) / 3 / 10;
            else
                blocksize = (tetrisFrame.getHeight()-50) /20;
            for (int i = 0; i < board.size(); i++) {
                for (int j = 0; j < board.get(i).length; j++) {
                    graphics2D.setColor(Color.black);
                    graphics2D.drawRect(j * blocksize, i * blocksize, blocksize, blocksize);
                    Color color = board.get(i)[j];
                    if (color != null) {
                        graphics2D.setColor(color);
                        graphics2D.fillRect(j * blocksize + 1, i * blocksize + 1, blocksize - 1, blocksize - 1);
                    } else {
                        graphics2D.setColor(Color.darkGray);
                        graphics2D.fillRect(j * blocksize + 1, i * blocksize + 1, blocksize - 1, blocksize - 1);
                    }
                }
            }
        }

        @Override
        public void propertyChange(PropertyChangeEvent e) {
            tetris.board = (LinkedList<Color[]>) e.getNewValue();

            if (model.getGameOverStatus()) {
                sidepanel.add(gameOver);
            } else {
                tetris.repaint();
                score.setText("Score: " + model.getScore());
                level.setText("Level: " + (model.getLevel() + 1));
                lines.setText("Lines: " + model.getCounter());
                nextlvl.setText("Next level: " + model.getGoal() + " lines");
                next.repaint();
            }
            sidepanel.setPreferredSize(new Dimension(tetrisFrame.getWidth()/3-20, tetrisFrame.getHeight()));
            sidepanel.updateUI();

            if (playerssize < model.getPlayers().size()) {
                for (int i = playerssize; i < model.getPlayers().size(); i++) {
                    sp[i] = new View.ServerPlayerPanel(model.getPlayers().get(i).getId());
                    sp[i].setAlignmentX(Component.LEFT_ALIGNMENT);
                    serverScores[i] = new JLabel("Player " + sp[i].id + ": " + sp[i].score);
                    serverScores[i].setAlignmentX(Component.CENTER_ALIGNMENT);
                    serverPanel.add(serverScores[i]);
                    serverPanel.add(sp[i]);
                }
                playerssize = model.getPlayers().size();
            }

            serverPanel.setPreferredSize(new Dimension(tetrisFrame.getWidth()/3, tetrisFrame.getHeight()));
            serverPanel.updateUI();

            for (int i = 0; i<playerssize; i++)
                sp[i].repaint();
        }
    }

    public class NextPiecePanel extends JPanel {
        Dimension size = new Dimension(130, 130);
        int blocksize = tetris.blocksize;

        NextPiecePanel() {
            super();
            setPreferredSize(size);
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D graphics2D = (Graphics2D) g;
            setPreferredSize(new Dimension((tetrisFrame.getHeight() - 50) / 20, (tetrisFrame.getHeight() - 50) / 20));
            blocksize = tetris.blocksize;
            Integer[][] shape = model.getNextPiece().turn(-1);
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    if (shape[i][j] == 1) {
                        graphics2D.setColor(Color.black);
                        graphics2D.drawRect(j * blocksize, i * blocksize, blocksize, blocksize);
                        graphics2D.setColor(model.getColors()[model.getNextid()]);
                        graphics2D.fillRect(j * blocksize + 1, i * blocksize + 1, blocksize - 1, blocksize - 1);
                    }
            sidepanel.setPreferredSize(new Dimension(tetrisFrame.getWidth()/3-20, tetrisFrame.getHeight()));
            sidepanel.updateUI();
        }
    }

    public class ServerPlayerPanel extends JPanel {
        ArrayList<Byte> board;
        int score;
        int id;
        int blocksize;

        ServerPlayerPanel(int id) {
            super();
            this.id = id;
            for (int i = 0; i<model.getPlayers().size(); i++)
                if (model.getPlayers().get(i).getId() == id) {
                    board = model.getPlayers().get(i).getField();
                    score = model.getPlayers().get(i).getScore();
                    break;
                }
            //получить список игроков из модели, привязать их id этому классу, получить из класса поле и счёт
            setPreferredSize(new Dimension(tetris.getWidth() / 2, tetris.getHeight() / 2));
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D graphics2D = (Graphics2D) g;

            setPreferredSize(new Dimension(tetris.getWidth() / 2, tetris.getHeight() / 2));
            blocksize = tetris.blocksize / 4;

            for (int i = 0; i<model.getPlayers().size(); i++)
                if (model.getPlayers().get(i).getId() == id) {
                    board = model.getPlayers().get(i).getField();
                    score = model.getPlayers().get(i).getScore();
                    serverScores[i].setText("Player's " + id + " score: " + score);
                    break;
                }

            for (int i = 0; i < board.size()/10; i++)
                for (int j = 0; j<10; j++) {
                    graphics2D.setColor(Color.black);
                    graphics2D.drawRect(j * blocksize, i * blocksize, blocksize, blocksize);

                    Color color;
                    if (board.get(i * 10 + j) != 8)
                        color = model.getColors()[board.get(i * 10 + j).intValue()];
                    else
                        color = Color.darkGray;
                    graphics2D.setColor(color);
                    graphics2D.fillRect(j * blocksize + 1, i * blocksize + 1, blocksize - 1, blocksize - 1);
                    serverPanel.setPreferredSize(new Dimension(tetrisFrame.getWidth() / 3, tetrisFrame.getHeight()));
                    serverPanel.updateUI();
                }
            serverPanel.updateUI();
        }
    }
}
