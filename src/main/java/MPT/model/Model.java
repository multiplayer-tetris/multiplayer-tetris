package MPT.model;

import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.*;

import javafx.util.Pair;
import MPT.service.Message;
import MPT.service.Protocol;
import MPT.service.Receiver;
import MPT.service.Sender;
import MPT.tetrominos.Tetromino;

import static java.lang.Math.*;

//TODO: @Model
// -- implement sequence request to server
// -- implement reserved sequence operations

public class Model {
    private static final int wight = 10;
    private static final int height = 20;
    private LinkedList<Color[]> coloredboard;
    private LinkedList<Color[]> boarddata;
    private LinkedList<Color[]> prev_boarddata;
    private String nickname;
    private boolean gameOver;
    private Timer stepTimer = null;
    private LinkedList<Integer> ids, reserveIds;
    private int id;
    private int nextid;
    private Color[] colors = {Color.cyan, Color.blue, Color.orange, Color.yellow, Color.green, Color.magenta.darker().darker(), Color.red, Color.LIGHT_GRAY};
    private HashMap<Color, Byte> colorByte;
    private int score;
    private int startlevel = 1;
    private int level;
    private int counter;
    private int goal;
    private int nextGoal;
    private boolean paused;
    private boolean internalpause;
    private final int[] delay = {800, 717, 633, 550, 467, 383, 300, 217, 133, 100, 83, 83, 83, 67, 67, 67, 50, 50, 50, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 17};
    private final Position spawn = new Position(3, 0);
    private Tetromino nextPiece;
    private MovableTetromino movablePiece;
    private final TetrominoFactory factory = new TetrominoFactory();
    private TetrisRNG tetrisRNG;
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private Model.TimerListener tl = null;
    private boolean down_pressed;
    private Map<Integer, Player> players;
    private InetAddress serverAddress;
    private Receiver receiver;
    private Sender sender;
    private byte gameId;
    private Long key;
    private int to_junk;
    private Vector<Integer> combos;
    private int from_junk;
    private int last_combo = -1;
    private int current_combo = -1;
    private int clear = 0;
    private boolean combo;
    private boolean ready;

    public Model() {
        super();
        key = System.currentTimeMillis();
        to_junk = 0;
        from_junk = 0;
        level = startlevel - 1;
        counter = 0;
        score = 0;
        goal = min(level * 10 + 10, max(100, 10 * level - 50));
        nextGoal = goal + 10;
        ids = new LinkedList<>();
        coloredboard = new LinkedList<>();
        boarddata = new LinkedList<>();
        players = new HashMap<>();
        for (int i = 0; i < height; i++)
            coloredboard.add(new Color[wight]);
        gameOver = false;
        this.colorByte = new HashMap<>();
        for (int i = 0; i < colors.length; ++i)
            this.colorByte.put(colors[i], (byte) i);
        this.colorByte.put(null, (byte) colors.length);

        ready = false;

        combos = new Vector<>();
        try {
            DatagramSocket socket = new DatagramSocket();
            this.receiver = new Receiver(socket);
            this.sender = new Sender(InetAddress.getByName("localhost"), socket);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public Model(InetAddress serverAddress) throws Exception {
        super();
        key = null;
        to_junk = 0;
        from_junk = 0;
        level = startlevel - 1;
        counter = 0;
        score = 0;
        goal = min(level * 10 + 10, max(100, 10 * level - 50));
        nextGoal = goal + 10;
        ids = new LinkedList<>();
        coloredboard = new LinkedList<>();
        boarddata = new LinkedList<>();
        players = new HashMap<>();
        for (int i = 0; i < height; i++)
            coloredboard.add(new Color[wight]);
        gameOver = false;
        this.serverAddress = serverAddress;
        this.colorByte = new HashMap<>();
        for (int i = 0; i < colors.length; ++i)
            this.colorByte.put(colors[i], (byte) i);
        this.colorByte.put(null, (byte) colors.length);
        try {
            DatagramSocket socket = new DatagramSocket();
            this.receiver = new Receiver(socket);
            this.sender = new Sender(serverAddress, socket);
            this.sender.start();
            this.sender.putMessage(new Message(Protocol.JOIN, null));

            this.receiver.start();
            boolean connect = this.receiver.waitForConnection();
            if (!connect) {
                receiver.kill();
                sender.kill();
                throw new Exception("Couldn't connect to server");
            }
        }
        catch (SocketException e){
            e.printStackTrace();
        }
        ready = false;
        Message msg = receiver.poll();
        while (msg != null) {
            handleMessage(msg);
            msg = receiver.poll();
        }

        combos = new Vector<>();
    }

    public void  newGame () {
        while (key == null) {
            Message msg = receiver.poll();
            while (msg != null) {
                handleMessage(msg);
                msg = receiver.poll();
            }
        }
        this.sender.putMessage(new Message(Protocol.START_GAME, null));
        level = startlevel - 1;
        counter = 0;
        score = 0;
        goal = min(level * 10 + 10, max(100, 10 * level - 50));
        nextGoal = goal + 10;
        coloredboard.clear();
        for (int h = 0; h < height; h++) {
            coloredboard.add(new Color[wight]);
            boarddata.add(new Color[wight]);
        }

        gameOver = false;
        paused = false;
        nickname = null;

        tetrisRNG = new TetrisRNG(key);
        ids = tetrisRNG.getSequence();
        reserveIds = tetrisRNG.getSequence();
        id = ids.getFirst();
        ids.removeFirst();
        nextid = ids.getFirst();
        ids.removeFirst();
        movablePiece = new MovableTetromino(factory.getTetromino(id), spawn);
        getBoardData();
        nextPiece = factory.getTetromino(nextid);
        if (tl == null)
            tl = new Model.TimerListener();
        if (stepTimer == null)
            stepTimer = new Timer(delay[level], tl);
        else
            stepTimer.setDelay(delay[level]);
        stepTimer.start();
        internalpause = false;
    }

    public void handleMessage(Message msg){
        switch (msg.getMsgType()){
            case Protocol.PLAYERS_FIELD:{
                Protocol.FieldTriplet triplet = (Protocol.FieldTriplet) msg.getData();
                byte id = triplet.getId();
                ArrayList<Byte> field = triplet.getField();
                int score = triplet.getScore();

                if (players.get((int)id) == null)
                    players.put((int) id, new Player(id));

                players.get((int)id).setField(field);
                players.get((int)id).setScore(score);
                break;
            }

            case Protocol.JOIN:{
                this.gameId = (byte)msg.getData();
                this.sender.setGameId(this.gameId);
                break;
            }

            case Protocol.COMBO:{
                //TODO
                System.out.println((int)msg.getData());
                this.from_junk += (int)msg.getData();
                sender.putMessage(new Message(Protocol.COMBO_RECVD, null));
                break;
            }

            case Protocol.COMBO_RECVD:{
                //TODO
                last_combo++;
                break;
            }

            case Protocol.START_GAME:{
                //TODO
                this.key = (Long)msg.getData();
                break;
            }
        }
    }

    private void step() {
        if ((movablePiece.getName().equals("MPT.tetrominos.I_piece") && movablePiece.getPosition().getY() == 0) && !movablePiece.getI_gimmik()) {
            movablePiece.setShape(movablePiece.getPiece().turn(0));
            getBoardData();
            movablePiece.setI_gimmik(true);
        } else
            down();
    }

    private void down() {
        // handle received messages
        Message msg = receiver.poll();
        while (msg != null) {
            handleMessage(msg);
            msg = receiver.poll();
        }

        if (current_combo > last_combo)
            sender.putMessage(packComboData());

        if (downIsEmpty(1, movablePiece.getShape())) {
            movablePiece.getPosition().changePosition(movablePiece.getPosition().getX(), movablePiece.getPosition().getY()+1);
            if (down_pressed)
                score++;
        } else {
            freeze(coloredboard);
            checkRows();
            junk(coloredboard);
            if (!(gameOver = isGameOver())) {
                movablePiece = new MovableTetromino(nextPiece, spawn);
                id = nextid;
                // supply `ids` with `reserveIds`
                // request for new reserve sequence
                if (ids.size() == 0) {
                    ids = reserveIds;
                    reserveIds = tetrisRNG.getSequence();
                    //sender.putMessage(new Message(Protocol.SEQUENCE_REQUEST, null));
                }
                nextid = ids.getFirst();
                ids.removeFirst();
                nextPiece = factory.getTetromino(nextid);

            } else {
                stepTimer.stop();
            }
        }
        getBoardData();
    }

    private void junk(LinkedList<Color[]> coloredboard) {
        int hole = new Random().nextInt(10);
        Color[] junk = new Color[wight];

        for (int i=0; i<wight; i++)
            if (i != hole)
                junk[i]=colors[7];

        LinkedList<Color[]> temp = new LinkedList<>();
        for (int i = 0; i < coloredboard.size()-from_junk; i++)
            temp.add(i, coloredboard.get(i+from_junk));
        for (int i = coloredboard.size()-from_junk; i<coloredboard.size(); i++) {
            temp.add(i, junk.clone());
        }

        for (int i = 0; i<height; i++)
            for (int j = 0; j<wight; j++)
                coloredboard.set(i, temp.get(i));
        from_junk = 0;
    }

    public boolean getPauseStatus() {
        return paused;
    }

    public boolean getInternalPauseStatus() {
        return internalpause;
    }

    public void movedown() {
        down_pressed = true;
        if (downIsEmpty(1, movablePiece.getShape()))
            down();
        down_pressed = false;
    }

    public void moveleft() {
        if (leftIsEmpty(1, movablePiece.getShape()))
            left(1);
        getBoardData();
    }

    public void moveright() {
        if (rightIsEmpty(1, movablePiece.getShape()))
            right(1);
        getBoardData();
    }

    public void drop() {
        if (!gameOver) {
            while (downIsEmpty(1, movablePiece.getShape())) {
                down();
                score += 2;
            }
            down();
        }
    }

    private void left(int n) {
        movablePiece.getPosition().setX(movablePiece.getPosition().getX()-n);
    }

    private void right(int n) {
        movablePiece.getPosition().setX(movablePiece.getPosition().getX()+n);
    }

    private void up(int n) {
        movablePiece.getPosition().setY(movablePiece.getPosition().getY()-n);
    }

    public void rotate(boolean clockwise) {
        if (turnIsPossible(clockwise)) {
            if (clockwise) {
                movablePiece.setState(movablePiece.getState()+1);
            }
            else {
                if (movablePiece.getState() == 0)
                    movablePiece.setState(3);
                else
                    movablePiece.setState(movablePiece.getState()-1);
            }
            movablePiece.setShape(movablePiece.getPiece().turn(movablePiece.getState()));
            movablePiece.setI_gimmik(true);
        }
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                if (movablePiece.getShape()[i][j] == 1) {
                    if (movablePiece.getPosition().getX() + j < 0)
                        right(abs(movablePiece.getPosition().getX() + j));
                    if (movablePiece.getPosition().getX() + j > 9)
                        left(movablePiece.getPosition().getX() + j - 9);
                    if (movablePiece.getPosition().getY() + i > 19)
                        up(movablePiece.getPosition().getY() + i - 19);
                }
        getBoardData();
    }

    private boolean leftIsEmpty(int n, Integer[][] shape) {
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                if (shape[i][j] == 1)
                    if (movablePiece.getPosition().getX() + j - n >= 0) {
                        if (coloredboard.get(movablePiece.getPosition().getY() + i)[movablePiece.getPosition().getX() + j - n] != null)
                            return false;
                    } else return false;
        return true;
    }

    private boolean rightIsEmpty(int n, Integer[][] shape) {
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                if (shape[i][j] == 1)
                    if (movablePiece.getPosition().getX() + j + n < 10) {
                        if (coloredboard.get(movablePiece.getPosition().getY() + i)[movablePiece.getPosition().getX() + j + n] != null)
                            return false;
                    } else return false;
        return true;
    }

    private boolean upIsEmpty(int n, Integer[][] shape) {
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                if (shape[i][j] == 1)
                    if (movablePiece.getPosition().getY() + i - n >= 0) {
                        if (coloredboard.get(movablePiece.getPosition().getY() + i - n)[movablePiece.getPosition().getX() + j] != null)
                            return false;
                    } else return false;
        return true;
    }

    private boolean downIsEmpty(int n, Integer[][] shape) {
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                if (shape[i][j] == 1)
                    if (movablePiece.getPosition().getY() + i + n < 20) {
                        if (coloredboard.get(movablePiece.getPosition().getY() + i + n)[movablePiece.getPosition().getX() + j] != null)
                            return false;
                    } else return false;
        return true;
    }

    private boolean turnIsPossible(boolean clockwise) {
        if (!movablePiece.getI_gimmik())
            movablePiece.setI_gimmik(true);
        Integer[][] tempShape;
        int tempState;
        if (clockwise) {
            tempState = movablePiece.getState() + 1;
        }
        else {
            if (movablePiece.getState() == 0)
                tempState = 3;
            else
                tempState = movablePiece.getState() - 1;
        }
        Tetromino tempPiece = movablePiece.getPiece();
        tempShape = tempPiece.turn(tempState);
        for (int i = 3; i >= 0; i--)
            for (int j = 3; j >= 0; j--)
                if (tempShape[i][j] == 1) {
                    if (movablePiece.getPosition().getX() + j > 9)
                        return leftIsEmpty(movablePiece.getPosition().getX() + j - 9, tempShape);
                    else if (movablePiece.getPosition().getY() + i > 19)
                        return upIsEmpty(movablePiece.getPosition().getY() + i - 19, tempShape);
                }
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                if (tempShape[i][j] == 1) {
                    if (movablePiece.getPosition().getX() + j < 0)
                        return rightIsEmpty(abs(movablePiece.getPosition().getX() + j), tempShape);
                    else if (movablePiece.getPosition().getY() + i < 0)
                        return downIsEmpty(abs(movablePiece.getPosition().getY() + i), tempShape);
                    if (coloredboard.get(movablePiece.getPosition().getY() + i)[movablePiece.getPosition().getX() + j] != null)
                        return false;
                }
        return true;
    }

    private void freeze(LinkedList<Color[]> board) {
        Color[] colortemp;
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                if (movablePiece.getShape()[i][j] == 1) {
                    colortemp = board.get(movablePiece.getPosition().getY() + i);
                    colortemp[movablePiece.getPosition().getX() + j] = colors[id];
                    board.set(movablePiece.getPosition().getY() + i, colortemp);
                }
    }

    private void checkRows() {
        if (clear != 0)
            combo = true;
        else combo = false;
        clear = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < wight; j++)
                if (coloredboard.get(i)[j] == null)
                    break;
                else if (j == 9) {
                    clear++;
                    counter++;
                    coloredboard.remove(i);
                    coloredboard.addFirst(new Color[wight]);
                }
        }

        switch (clear) {
            case 1:
                score += (level + 1) * 100;
                break;
            case 2:
                score += (level + 1) * 300;
                break;
            case 3:
                score += (level + 1) * 500;
                break;
            case 4:
                score += (level + 1) * 800;
                break;
            default: {
            }
        }

        if (combo)
            to_junk = clear;
        else
            to_junk = clear-1;

        if (to_junk>=from_junk) {
            from_junk = 0;
            to_junk = to_junk - from_junk;
        }
        else {
            to_junk = 0;
            from_junk = from_junk - to_junk;
        }

        if (to_junk != 0) {
            combos.add(to_junk);
            current_combo++;
        }

        if (counter >= goal) {
            goal = nextGoal;
            nextGoal = goal + 10;
            level++;
            if (level < 29) {
                stepTimer.setDelay(delay[level]);
                stepTimer.start();
            }
        }
    }

    private boolean isGameOver() {
        for (int i = spawn.getY(); i < spawn.getY() + 2; i++)
            for (int j = spawn.getX(); j < spawn.getX() + 4; j++)
                if (coloredboard.get(i)[j] != null)
                    return true;
        return false;
    }

    public boolean getGameOverStatus() {
        return gameOver;
    }

    public void getBoardData() {
        prev_boarddata = (LinkedList<Color[]>) boarddata.clone();
        boarddata.clear();
        for (final Color[] row : coloredboard)
            boarddata.add(row.clone());
        freeze(boarddata);
        setProperty();

        // packing this player's board data
        // and send to server
        Message msg = packBoardData();
        sender.putMessage(msg);
    }

    private Message packBoardData(){
        byte[] buf = new byte[206];
        buf[0] = Protocol.PLAYERS_FIELD;
        int i = 2;
        for (Color[] row : boarddata) {
            for (Color color : row) {
                buf[i++] = colorByte.get(color);
            }
        }

        byte[] scoreBuf = ByteBuffer.allocate(Integer.BYTES).putInt(score).array();
        System.arraycopy(scoreBuf, 0, buf, 202, Integer.BYTES);

        return new Message(Protocol.PLAYERS_FIELD, buf);
    }

    private Message packComboData() {
        return new Message(Protocol.COMBO, new Pair<Integer, Integer>(last_combo+1, combos.get(last_combo+1)));
    }

    public int getScore() {
        return score;
    }

    public int getLevel() {
        return level;
    }

    public int getCounter() {
        return counter;
    }

    public int getGoal() {
        return goal;
    }

    public int getHeight() {
        return height;
    }

    public int getWight() {
        return wight;
    }

    public Tetromino getNextPiece() {
        return nextPiece;
    }

    public Color[] getColors() {
        return colors;
    }

    public int getNextid() {
        return nextid;
    }

    private class TimerListener implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
            step();
            //Обработка пакетов при окончании игры
        }
    }

    public void addObserver(PropertyChangeListener l) {
        pcs.addPropertyChangeListener("Board", l);
    }

    private void setProperty() {
        pcs.firePropertyChange("Board", prev_boarddata, boarddata);
    }

    public LinkedList<Player> getPlayers(){
        return new LinkedList<Player>(players.values());
    }

    public Map getHashMapPlayers() {return players;}

    public boolean getReady () {
        return ready;
    }

    public Sender getSender() {
        return sender;
    }

    public int getTo_junk() {
        return to_junk;
    }

    public int getFrom_junk() {
        return from_junk;
    }

    public void setFrom_junk(int from_junk) {
        this.from_junk = from_junk;
    }

    public LinkedList<Color[]> getColoredboard() {
        return coloredboard;
    }

    public HashMap<Color, Byte> getColorByte() {
        return colorByte;
    }

    public LinkedList<Color[]> getBoarddata() {
        return boarddata;
    }

    public Vector<Integer> getCombos() {
        return combos;
    }

    public void setKey(Long key) {
        this.key = key;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public int getNextGoal() {
        return nextGoal;
    }

    public void setTetrisRNG(TetrisRNG tetrisRNG) {
        this.tetrisRNG = tetrisRNG;
    }

    public int getId() {
        return id;
    }

    public Position getSpawn() {
        return spawn;
    }

    public MovableTetromino getMovablePiece() {
        return movablePiece;
    }

    public int getNextId() {
        return nextid;
    }

    public void setMovablePiece(MovableTetromino movablePiece) {
        this.movablePiece = movablePiece;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public void setNextPiece(Tetromino nextPiece) {
        this.nextPiece = nextPiece;
    }

    public void setIds(LinkedList<Integer> ids) {
        this.ids = ids;
    }
}