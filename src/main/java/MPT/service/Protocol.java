package MPT.service;

// Protocol description @ `PROTO.txt`

import javafx.util.Pair;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedList;

public class Protocol {
    public final static byte JOIN = 0X00;
    public final static byte PLAYERS_FIELD = 0X01;
    public final static byte COMBO = 0x02;
    public final static byte COMBO_RECVD = 0x03;
    public final static byte READINESS = 0x04;
    public final static byte START_GAME = 0x05;
    public final static byte KILL_SENDER = 0x06;


    /**
     * Used for extracting message representation at client`s
     * end.
     *
     * @param buf
     * @return
     * @throws IllegalArgumentException
     */
    public static Message extractMessage(byte[] buf) throws IllegalArgumentException{
        switch (buf[0]){
            case JOIN: return new Message(JOIN, buf[1]);
            case PLAYERS_FIELD: return new Message(PLAYERS_FIELD,
                    extractFieldMessage(buf));
            case COMBO: {
                int lines = ByteBuffer.wrap(buf,1, 4).getInt();
                return new Message(COMBO, lines);
            }
            case COMBO_RECVD: return new Message(COMBO_RECVD, null);
            case START_GAME: {
                long key = ByteBuffer.wrap(buf, 1, 8).getLong();
                return new Message(START_GAME, key);
            }
        }

        return null;
    }

    public static FieldTriplet extractFieldMessage(byte[] buf){
        ArrayList<Byte> field = extractField(buf);
        byte id = buf[1];
        int score = ByteBuffer.wrap(buf, 202, 4).getInt();

        return new FieldTriplet(field, id, score);
    }

    public static ArrayList<Byte> extractField(byte[] buf){
        // invalid field data
        if (buf.length < 202)
            throw new IllegalArgumentException("Invalid buffer size");

        ArrayList<Byte> result = new ArrayList<>();
        for (int i = 2; i < 200 + 2; ++i)
            result.add(buf[i]);

        return result;
    }

    public static class FieldTriplet{
        private final ArrayList<Byte> field;
        private final byte id;
        private final int score;

        public FieldTriplet(ArrayList<Byte> field, byte id, int score) {
            this.field = field;
            this.id = id;
            this.score = score;
        }

        public ArrayList<Byte> getField() {
            return field;
        }

        public byte getId() {
            return id;
        }

        public int getScore() {
            return score;
        }
    }
}
