package MPT.tetrominos;

public interface Tetromino {
    abstract Integer[][] turn(int state);
}
