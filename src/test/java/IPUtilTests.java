import MPT.IPUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import static MPT.IPUtil.validIP;
import static org.junit.jupiter.api.Assertions.*;

public class IPUtilTests {

    @Test
    @DisplayName("IPUtilTest")
    public void ipUtilTest() {
        assertFalse(validIP(""));
        assertFalse(validIP(null));

        assertFalse(validIP("123.123"));
        assertFalse(validIP("123.123.123.123."));

        assertFalse(validIP("-5.123.123.123"));
        assertFalse(validIP("300.123.123.123"));

        assertTrue(validIP("123.123.123.123"));
    }
}
