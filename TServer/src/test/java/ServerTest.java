import org.junit.*;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.io.IOException;
import java.net.*;
import java.util.Arrays;

import org.junit.*;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.net.InetAddress;

public class ServerTest {

    private Player player = new Player();

    byte id = 1;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void globalSetUp() {
        System.out.println("Initial setup...");
        System.out.println("Code executes only once");
    }

    @Test
    public void checkReceivingUTP() throws IOException {
        try {
            int serverSocketUDP = 7000;
            byte[] sendingDataBuffer = new byte[1024];
            byte[] receivingDataBuffer = new byte[1024];
            DatagramSocket clientSocket = new DatagramSocket();
            InetAddress IPAddress = InetAddress.getByName("localhost");
            Server server = new Server(serverSocketUDP);
            sendingDataBuffer[0] = 10;
            DatagramPacket sendingPacket = new DatagramPacket(sendingDataBuffer,sendingDataBuffer.length,IPAddress, serverSocketUDP);
            clientSocket.send(sendingPacket);
            server.receivingUTP();
            byte[] data = server.getReceivingDataBufferUDP();
            assertThat(data[0], is(sendingDataBuffer[0]));
        }
        catch(SocketException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void checkJoiningGameUDP() throws IOException {
        try {
            int serverSocketUDP = 7001;
            byte[] sendingDataBuffer = new byte[1024];
            byte[] receivingDataBuffer = new byte[1024];
            DatagramSocket clientSocket = new DatagramSocket();
            InetAddress IPAddress = InetAddress.getByName("localhost");
            Server server = new Server(serverSocketUDP);
            sendingDataBuffer[0] = 0;
            DatagramPacket sendingPacket = new DatagramPacket(sendingDataBuffer,sendingDataBuffer.length,IPAddress, serverSocketUDP);
            clientSocket.send(sendingPacket);
            server.receivingUTP();
            server.joiningGameUDP(player, id);
            DatagramPacket receivingPacket = new DatagramPacket(receivingDataBuffer,receivingDataBuffer.length);
            clientSocket.receive(receivingPacket);
            assertThat(receivingDataBuffer[0], is(sendingDataBuffer[0]));
            assertThat(receivingDataBuffer[1], is(id));
        }
        catch(SocketException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void checkgetReceivingDataBufferUDP() throws IOException {
        try {
            int serverSocketUDP = 7002;
            byte[] sendingDataBuffer = new byte[1024];
            byte[] receivingDataBuffer = new byte[1024];
            DatagramSocket clientSocket = new DatagramSocket();
            InetAddress IPAddress = InetAddress.getByName("localhost");
            Server server = new Server(serverSocketUDP);
            sendingDataBuffer[0] = 0;
            DatagramPacket sendingPacket = new DatagramPacket(sendingDataBuffer,sendingDataBuffer.length,IPAddress, serverSocketUDP);
            clientSocket.send(sendingPacket);
            server.receivingUTP();
            server.joiningGameUDP(player, id);
            DatagramPacket receivingPacket = new DatagramPacket(receivingDataBuffer,receivingDataBuffer.length);
            clientSocket.receive(receivingPacket);
            sendingDataBuffer[0] = 1;
            for(int i = 1; i < 201; i++){
                sendingDataBuffer[i] = 2;
            }
            clientSocket.send(sendingPacket);
            server.receivingUTP();
            server.playerField(player);
            clientSocket.receive(receivingPacket);
            assertThat(receivingDataBuffer, is(sendingDataBuffer));
        }
        catch(SocketException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void checkStart() throws IOException {
        try {
            int serverSocketUDP = 7003;
            byte[] sendingDataBuffer = new byte[1024];
            byte[] receivingDataBuffer = new byte[1024];
            DatagramSocket clientSocket = new DatagramSocket();
            InetAddress IPAddress = InetAddress.getByName("localhost");
            Server server = new Server(serverSocketUDP);
            sendingDataBuffer[0] = 0;
            DatagramPacket sendingPacket = new DatagramPacket(sendingDataBuffer,sendingDataBuffer.length,IPAddress, serverSocketUDP);
            clientSocket.send(sendingPacket);
            server.receivingUTP();
            server.joiningGameUDP(player, id);
            DatagramPacket receivingPacket = new DatagramPacket(receivingDataBuffer,receivingDataBuffer.length);
            clientSocket.receive(receivingPacket);
            server.start(player);
            clientSocket.receive(receivingPacket);
            byte code = 5;
            assertThat(receivingDataBuffer[0], is(code));
        }
        catch(SocketException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void checkCombo() throws IOException {
        try {
            int serverSocketUDP = 7004;
            byte[] sendingDataBuffer = new byte[1024];
            byte[] receivingDataBuffer = new byte[1024];
            DatagramSocket clientSocket = new DatagramSocket();
            InetAddress IPAddress = InetAddress.getByName("localhost");
            Server server = new Server(serverSocketUDP);
            sendingDataBuffer[0] = 0;
            DatagramPacket sendingPacket = new DatagramPacket(sendingDataBuffer,sendingDataBuffer.length,IPAddress, serverSocketUDP);
            clientSocket.send(sendingPacket);
            server.receivingUTP();
            server.joiningGameUDP(player, id);
            DatagramPacket receivingPacket = new DatagramPacket(receivingDataBuffer,receivingDataBuffer.length);
            clientSocket.receive(receivingPacket);
            byte[] data = {2, 1, 0, 0, 0, 1, 2, 2, 2, 2};
            server.combo(player, data);
            clientSocket.receive(receivingPacket);
            byte[] dataTest = new byte[1024];
            for(int i = 0; i < 5; i++){
                dataTest[i] = 2;
            }
            assertThat(receivingDataBuffer, is(dataTest));
        }
        catch(SocketException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void checkComboReceived() throws IOException {
        try {
            int serverSocketUDP = 7005;
            byte[] sendingDataBuffer = new byte[1024];
            byte[] receivingDataBuffer = new byte[1024];
            DatagramSocket clientSocket = new DatagramSocket();
            InetAddress IPAddress = InetAddress.getByName("localhost");
            Server server = new Server(serverSocketUDP);
            sendingDataBuffer[0] = 0;
            DatagramPacket sendingPacket = new DatagramPacket(sendingDataBuffer,sendingDataBuffer.length,IPAddress, serverSocketUDP);
            clientSocket.send(sendingPacket);
            server.receivingUTP();
            server.joiningGameUDP(player, id);
            DatagramPacket receivingPacket = new DatagramPacket(receivingDataBuffer,receivingDataBuffer.length);
            clientSocket.receive(receivingPacket);
            server.comboReceived(player);
            clientSocket.receive(receivingPacket);
            byte code = 3;
            assertThat(receivingDataBuffer[0], is(code));
        }
        catch(SocketException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void checkLongToBytes() {
        int serverSocketUDP = 7006;
        Server server = new Server(serverSocketUDP);
        byte[] data = server.longToBytes(1);
        byte[] dataTest = {0, 0, 0, 0, 0, 0, 0, 1};
        assertThat(data, is(dataTest));
    }

}
