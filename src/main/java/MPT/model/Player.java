package MPT.model;

import java.util.ArrayList;

public class Player {
    private final int id;
    private int score;
    private ArrayList<Byte> field;

    Player(int id){
        this.id = id;
        this.score = 0;
        this.field = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public int getScore() {
        return score;
    }

    public ArrayList<Byte> getField() {
        return field;
    }

    public void setScore(int score){
        this.score = score;
    }

    public void setField(ArrayList<Byte> field) throws IllegalArgumentException{
        if (field.size() != 200)
            throw new IllegalArgumentException("Required field size is 20 x 10.");
        this.field = field;
    }
}
